package salariati.validator;

import salariati.model.DidacticFunction;
import salariati.model.Employee;

public class EmployeeValidator {
	
	public EmployeeValidator(){}

	public boolean isValid(Employee employee) {
		boolean isFirstNameValid  = employee.getFirstName()!=null&&employee.getFirstName().matches("[a-zA-Z]+") && (employee.getFirstName().length() > 2) && (employee.getFirstName().length() < 100);
		boolean isLastNameValid  = employee.getLastName()!=null&&employee.getLastName().matches("[a-zA-Z]+") && (employee.getLastName().length() > 2)&& (employee.getLastName().length() < 100);
		boolean isCNPValid       = employee.getCnp()!=null&&employee.getCnp().matches("[0-9]+") && (employee.getCnp().length() == 13);
		boolean isFunctionValid  = employee.getFunction()!=null&&(employee.getFunction().equals(DidacticFunction.ASISTENT) ||
								   employee.getFunction().equals(DidacticFunction.LECTURER) ||
								   employee.getFunction().equals(DidacticFunction.TEACHER));
		boolean isSalaryValid    = employee.getSalary()!=null&&employee.getSalary().matches("[0-9]+") && (Integer.parseInt(employee.getSalary()) <100000) && (Integer.parseInt(employee.getSalary()) > 0);
		
		return isFirstNameValid && isLastNameValid && isCNPValid && isFunctionValid && isSalaryValid;
	}

	
}
