package salariati.repository.implementations;

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import salariati.exception.EmployeeException;

import salariati.model.Employee;

import salariati.repository.interfaces.EmployeeRepositoryInterface;
import salariati.validator.EmployeeValidator;

public class EmployeeRepositoryDB implements EmployeeRepositoryInterface {
	
	private final String employeeDBFile = "employees.txt";
	private EmployeeValidator employeeValidator = new EmployeeValidator();

	@Override
	public boolean addEmployee(Employee employee) {
		if( employeeValidator.isValid(employee) ) {
			BufferedWriter bw = null;
			try {
				bw = new BufferedWriter(new FileWriter(employeeDBFile, true));
				bw.write(employee.toString());
				bw.newLine();
				bw.close();
				return true;
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return false;
	}

	@Override
	public void deleteEmployee(final Employee employee) {
		List<Employee> employeeList = getEmployeeList();
		employeeList.removeIf(e->e.equals(employee));
		BufferedWriter bw = null;
		try {
			bw = new BufferedWriter(new FileWriter(employeeDBFile, false));
			for (Employee e:employeeList
				 ) {

			bw.write(e.toString());
			bw.newLine();
			}
			bw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}

	@Override
	public void modifyEmployee(Employee oldEmployee, Employee newEmployee) {
		if( employeeValidator.isValid(newEmployee) ) {
			List<Employee> employeeList = getEmployeeList();
			int index = -1;
			for (int i = 0; i < employeeList.size(); i++)
				if (employeeList.get(i).equals(oldEmployee))
					index = i;
			if (index != -1) {
				employeeList.set(index, newEmployee);
				BufferedWriter bw = null;
				try {
					bw = new BufferedWriter(new FileWriter(employeeDBFile, false));
					for (Employee e : employeeList
							) {

						bw.write(e.toString());
						bw.newLine();
					}
					bw.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		
	}

	@Override
	public List<Employee> getEmployeeList() {
		List<Employee> employeeList = new ArrayList<Employee>();
		
		BufferedReader br = null;
		try {
			br = new BufferedReader(new FileReader(employeeDBFile));
			String line;
			int counter = 0;
			while ((line = br.readLine()) != null) {
				Employee employee = new Employee();
				try {
					employee = Employee.getEmployeeFromString(line, counter);
					employeeList.add(employee);
				} catch(EmployeeException ex) {
					System.err.println("Error while reading: " + ex.toString());
				}
				counter++;
			}
		} catch (FileNotFoundException e) {
			System.err.println("Error while reading: " + e);
		} catch (IOException e) {
			System.err.println("Error while reading: " + e);
		} finally {
			if (br != null)
				try {
					br.close();
				} catch (IOException e) {
					System.err.println("Error while closing the file: " + e);
				}
		}
		
		return employeeList;
	}

    public List<Employee> getOrderedEmployeeList() {
        List<Employee> employeeList = getEmployeeList();

        Collections.sort(employeeList,(a,b)->{
            int c=b.getSalary().compareTo(a.getSalary());
            if(c==0)
                c=a.getCnp().compareTo(b.getCnp());
            return c;
        });
        return employeeList;
    }



}
