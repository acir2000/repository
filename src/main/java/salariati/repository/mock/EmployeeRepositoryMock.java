package salariati.repository.mock;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import salariati.model.DidacticFunction;

import salariati.model.Employee;
import salariati.repository.interfaces.EmployeeRepositoryInterface;
import salariati.validator.EmployeeValidator;

public class EmployeeRepositoryMock implements EmployeeRepositoryInterface {

	private List<Employee> employeeList;

    public void setEmployeeList(List<Employee> employeeList) {
        this.employeeList = employeeList;
    }

    private EmployeeValidator employeeValidator;
	
	public EmployeeRepositoryMock() {
		
		employeeValidator = new EmployeeValidator();
		employeeList = new ArrayList<Employee>();

		
		Employee Ionel   = new Employee("Pacuraru","Ionel", "1234567890876", DidacticFunction.ASISTENT, "2500");
		Employee Mihai   = new Employee("Dumitrescu","Mihai", "1234567890876", DidacticFunction.LECTURER, "2500");
		Employee Ionela  = new Employee("Ionescu","Ionela", "1234567890876", DidacticFunction.LECTURER, "2500");
		Employee Mihaela = new Employee("Pacuraru","Mihaela", "1234567890876", DidacticFunction.ASISTENT, "2500");
		Employee Vasile  = new Employee("Georgescu","Vasile", "1234567890876", DidacticFunction.TEACHER,  "2500");
		Employee Marin   = new Employee("Puscas","Marin", "1234567890876", DidacticFunction.TEACHER,  "2500");
		
		employeeList.add( Ionel );
		employeeList.add( Mihai );
		employeeList.add( Ionela );
		employeeList.add( Mihaela );
		employeeList.add( Vasile );
		employeeList.add( Marin );

	}

	public void clearRepositoryMock(){
		employeeList.clear();
	}
	
	@Override
	public boolean addEmployee(Employee employee) {
		if ( employeeValidator.isValid(employee)) {
			employeeList.add(employee);
			return true;
		}
		return false;
	}
	
	@Override
	public void deleteEmployee(Employee employee) {
		employeeList.removeIf(e->e.equals(employee));
	}

	@Override
	public void modifyEmployee(Employee oldEmployee, Employee newEmployee) {
		if ( employeeValidator.isValid(newEmployee)) {
			int index = -1;
			for (int i = 0; i < employeeList.size(); i++)
				if (employeeList.get(i).equals(oldEmployee))
					index = i;
			if (index != -1) {
				employeeList.set(index, newEmployee);
			}
		}
	}

	@Override
	public List<Employee> getEmployeeList() {
		return employeeList;
	}


	@Override
	public List<Employee> getOrderedEmployeeList(){
		Collections.sort(employeeList,(a, b)->{
			int c=b.getSalary().compareTo(a.getSalary());
			if(c==0)
				c=a.getCnp().compareTo(b.getCnp());
			return c;
		});
		return employeeList;
	}

}
