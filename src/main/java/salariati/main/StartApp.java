package salariati.main;

import salariati.model.Employee;
import salariati.repository.implementations.EmployeeRepositoryDB;
import salariati.repository.interfaces.EmployeeRepositoryInterface;
import salariati.controller.EmployeeController;
import salariati.model.DidacticFunction;

import java.util.Scanner;

//functionalitati
//i.	 adaugarea unui nou angajat (nume, prenume, CNP, functia didactica, salariul de incadrare);
//ii.	 modificarea functiei didactice (asistent/lector/conferentiar/profesor) a unui angajat;
//iii.	 afisarea salariatilor ordonati descrescator dupa salariu si crescator dupa varsta (CNP).
public class StartApp {
	
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);

		EmployeeRepositoryInterface employeesRepository = new EmployeeRepositoryDB();
		EmployeeController employeeController = new EmployeeController(employeesRepository);

		String option="";

		while(true){
			System.out.println("MENIU\n1-Print ordered list\n2-Add new employee\n3-Delete employee\n4-Edit employee\n5-Exit");
			option=sc.nextLine();
			if(option.equals("1")){
				for(Employee _employee : employeeController.getOrderedEmployeesList())
					System.out.println(_employee.toString());
				System.out.println("-----------------------------------------");
			}
			else if(option.equals("2")){
				System.out.println("First name:");
				String firstName=sc.nextLine();
				System.out.println("Last name:");
				String lastName=sc.nextLine();
				System.out.println("CNP:");
				String cnp=sc.nextLine();
				System.out.println("Didactic function:");
				String function=sc.nextLine();
				DidacticFunction function1=null;
				if(function.equals("ASISTENT"))
					function1=DidacticFunction.ASISTENT;
				if(function.equals("LECTURER"))
					function1=DidacticFunction.LECTURER;
				if(function.equals("TEACHER"))
					function1=DidacticFunction.TEACHER;
				System.out.println("Salary:");
				String salary=sc.nextLine();
				Employee employee = new Employee(firstName,lastName, cnp, function1, salary);
				employeeController.addEmployee(employee);

			}
			else if(option.equals("3")){
				System.out.println("First name:");
				String firstName=sc.nextLine();
				System.out.println("Last name:");
				String lastName=sc.nextLine();
				System.out.println("CNP:");
				String cnp=sc.nextLine();
				System.out.println("Didactic function:");
				String function=sc.nextLine();
				DidacticFunction function1=null;
				if(function.equals("ASISTENT"))
					function1=DidacticFunction.ASISTENT;
				if(function.equals("LECTURER"))
					function1=DidacticFunction.LECTURER;
				if(function.equals("TEACHER"))
					function1=DidacticFunction.TEACHER;
				System.out.println("Salary:");
				String salary=sc.nextLine();
				Employee employee = new Employee(firstName,lastName, cnp, function1, salary);
				employeeController.deleteEmployee(employee);
			}
			else if(option.equals("4")){
				System.out.println("Old employee data:\n");
				System.out.println("First name:");
				String firstName=sc.nextLine();
				System.out.println("Last name:");
				String lastName=sc.nextLine();
				System.out.println("CNP:");
				String cnp=sc.nextLine();
				System.out.println("Didactic function:");
				String function=sc.nextLine();
				DidacticFunction function1=null;
				if(function.equals("ASISTENT"))
					function1=DidacticFunction.ASISTENT;
				if(function.equals("LECTURER"))
					function1=DidacticFunction.LECTURER;
				if(function.equals("TEACHER"))
					function1=DidacticFunction.TEACHER;
				System.out.println("Salary:");
				String salary=sc.nextLine();
				Employee employee = new Employee(firstName,lastName, cnp, function1, salary);

				System.out.println("New employee data:\n");
				System.out.println("First name:");
				String firstName1=sc.nextLine();
				System.out.println("Last name:");
				String lastName1=sc.nextLine();
				System.out.println("CNP:");
				String cnp1=sc.nextLine();
				System.out.println("Didactic function:");
				String function2=sc.nextLine();
				DidacticFunction function3=null;
				if(function2.equals("ASISTENT"))
					function3=DidacticFunction.ASISTENT;
				if(function2.equals("LECTURER"))
					function3=DidacticFunction.LECTURER;
				if(function2.equals("TEACHER"))
					function3=DidacticFunction.TEACHER;
				System.out.println("Salary:");
				String salary1=sc.nextLine();
				Employee employee1 = new Employee(firstName1,lastName1, cnp1, function3, salary1);
				employeeController.modifyEmployee(employee,employee1);
			}
			else if(option.equals("5")){
				break;
			}

		}



	}

}
