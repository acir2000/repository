package salariati.test;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import salariati.model.DidacticFunction;
import salariati.model.Employee;
import salariati.repository.mock.EmployeeRepositoryMock;
import salariati.validator.EmployeeValidator;

import java.util.List;

import static org.junit.Assert.assertEquals;

class SortEmployeeRepositoryMockTest {

    private EmployeeRepositoryMock employeeRepository;
    private EmployeeValidator employeeValidator;

    @BeforeEach
    void setUp() {
        employeeRepository = new EmployeeRepositoryMock();
        employeeRepository.clearRepositoryMock();
        employeeValidator  = new EmployeeValidator();
    }



    @Test
    void sortEmployeeTCValid() {
        Employee employee1 = new Employee("Adam","Corina", "1234567890123", DidacticFunction.LECTURER, "2000");
        Employee employee2 = new Employee("Adam","Mihai", "1234567890124", DidacticFunction.ASISTENT, "2500");

        employeeRepository.addEmployee(employee1);
        employeeRepository.addEmployee(employee2);
        List<Employee> sortedList=employeeRepository.getOrderedEmployeeList();
        assertEquals(2, sortedList.size());
        assertEquals(employee2,sortedList.get(0));
        assertEquals(employee1,sortedList.get(1));

    }

    @Test
    void sortEmployeeTCNonValid() {
        List<Employee> sortedList=employeeRepository.getOrderedEmployeeList();
        assertEquals(0, employeeRepository.getEmployeeList().size());
    }


  }