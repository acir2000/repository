package salariati.test;

import static org.junit.Assert.*;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

import org.junit.jupiter.api.BeforeEach;
import salariati.model.Employee;

import org.junit.Before;
import org.junit.Test;

import salariati.repository.interfaces.EmployeeRepositoryInterface;
import salariati.repository.mock.EmployeeRepositoryMock;
import salariati.validator.EmployeeValidator;
import salariati.controller.EmployeeController;
import salariati.model.DidacticFunction;

public class AddEmployeeTest {

	private EmployeeRepositoryInterface employeeRepository;
	private EmployeeValidator employeeValidator;

	@BeforeEach
	void setUp() {
		employeeRepository = new EmployeeRepositoryMock();
		employeeValidator  = new EmployeeValidator();
	}



	@org.junit.jupiter.api.Test
	void addEmployeeEC1() {
		Employee newEmployee = new Employee("Adam","Corina", "1234567890123", DidacticFunction.LECTURER, "2000");
		assertTrue(employeeValidator.isValid(newEmployee));
		assertTrue(employeeRepository.addEmployee(newEmployee)==true);
		assertEquals(7, employeeRepository.getEmployeeList().size());
		assertTrue(newEmployee.equals(employeeRepository.getEmployeeList().get(employeeRepository.getEmployeeList().size() - 1)));
	}

	@org.junit.jupiter.api.Test
	void addEmployeeEC3() {
		Employee newEmployee = new Employee("A2am","Corina", "1234567890123", DidacticFunction.LECTURER, "2000");
		assertFalse(employeeValidator.isValid(newEmployee));
		assertFalse(employeeRepository.addEmployee(newEmployee)==true);
		assertNotEquals(7, employeeRepository.getEmployeeList().size());
		assertFalse(newEmployee.equals(employeeRepository.getEmployeeList().get(employeeRepository.getEmployeeList().size() - 1)));
	}

	@org.junit.jupiter.api.Test
	void addEmployeeEC4() {
		Employee newEmployee = new Employee("Ad","Corina", "1234567890123", DidacticFunction.LECTURER, "2000");
		assertFalse(employeeValidator.isValid(newEmployee));
		assertFalse(employeeRepository.addEmployee(newEmployee)==true);
		assertNotEquals(7, employeeRepository.getEmployeeList().size());
		assertFalse(newEmployee.equals(employeeRepository.getEmployeeList().get(employeeRepository.getEmployeeList().size() - 1)));
	}

	@org.junit.jupiter.api.Test
	void addEmployeeEC6() {
		Employee newEmployee = new Employee("Adam","C0rina", "1234567890123", DidacticFunction.LECTURER, "2000");
		assertFalse(employeeValidator.isValid(newEmployee));
		assertFalse(employeeRepository.addEmployee(newEmployee)==true);
		assertNotEquals(7, employeeRepository.getEmployeeList().size());
		assertFalse(newEmployee.equals(employeeRepository.getEmployeeList().get(employeeRepository.getEmployeeList().size() - 1)));
	}

	@org.junit.jupiter.api.Test
	void addEmployeeEC11() {
		Employee newEmployee = new Employee("Adam","Corina", "123456789aaaa", DidacticFunction.LECTURER, "2000");
		assertFalse(employeeValidator.isValid(newEmployee));
		assertFalse(employeeRepository.addEmployee(newEmployee)==true);
		assertNotEquals(7, employeeRepository.getEmployeeList().size());
		assertFalse(newEmployee.equals(employeeRepository.getEmployeeList().get(employeeRepository.getEmployeeList().size() - 1)));
	}

	@org.junit.jupiter.api.Test
	void addEmployeeEC16() {
		Employee newEmployee = new Employee("Adam","Corina", "1234567890123", DidacticFunction.LECTURER, "-100");
		assertFalse(employeeValidator.isValid(newEmployee));
		assertFalse(employeeRepository.addEmployee(newEmployee)==true);
		assertNotEquals(7, employeeRepository.getEmployeeList().size());
		assertFalse(newEmployee.equals(employeeRepository.getEmployeeList().get(employeeRepository.getEmployeeList().size() - 1)));
	}

	@org.junit.jupiter.api.Test
	void addEmployeeBVA4() {
		Employee newEmployee = new Employee("A","Corina", "1234567890123", DidacticFunction.LECTURER, "100");
		assertFalse(employeeValidator.isValid(newEmployee));
		assertFalse(employeeRepository.addEmployee(newEmployee)==true);
		assertNotEquals(7, employeeRepository.getEmployeeList().size());
		assertFalse(newEmployee.equals(employeeRepository.getEmployeeList().get(employeeRepository.getEmployeeList().size() - 1)));
	}

	@org.junit.jupiter.api.Test
	void addEmployeeBVA13() {
		Employee newEmployee = new Employee("Adam","Ma", "1234567890123", DidacticFunction.LECTURER, "100");
		assertFalse(employeeValidator.isValid(newEmployee));
		assertFalse(employeeRepository.addEmployee(newEmployee)==true);
		assertNotEquals(7, employeeRepository.getEmployeeList().size());
		assertFalse(newEmployee.equals(employeeRepository.getEmployeeList().get(employeeRepository.getEmployeeList().size() - 1)));
	}

	@org.junit.jupiter.api.Test
	void addEmployeeBVA20() {
		Employee newEmployee = new Employee("Adam","Mara", "123456789012", DidacticFunction.LECTURER, "100");
		assertFalse(employeeValidator.isValid(newEmployee));
		assertFalse(employeeRepository.addEmployee(newEmployee)==true);
		assertNotEquals(7, employeeRepository.getEmployeeList().size());
		assertFalse(newEmployee.equals(employeeRepository.getEmployeeList().get(employeeRepository.getEmployeeList().size() - 1)));
	}

	@org.junit.jupiter.api.Test
	void addEmployeeBVA24() {
		Employee newEmployee = new Employee("Adam","Corina", "1234567890123", DidacticFunction.ASISTENT, "200");
		assertTrue(employeeValidator.isValid(newEmployee));
		assertTrue(employeeRepository.addEmployee(newEmployee)==true);
		assertEquals(7, employeeRepository.getEmployeeList().size());
		assertTrue(newEmployee.equals(employeeRepository.getEmployeeList().get(employeeRepository.getEmployeeList().size() - 1)));
	}

	@org.junit.jupiter.api.Test
	void addEmployeeBVA33() {
		Employee newEmployee = new Employee("Adam","Mara", "1234567890123", DidacticFunction.LECTURER, "100001");
		assertFalse(employeeValidator.isValid(newEmployee));
		assertFalse(employeeRepository.addEmployee(newEmployee)==true);
		assertNotEquals(7, employeeRepository.getEmployeeList().size());
		assertFalse(newEmployee.equals(employeeRepository.getEmployeeList().get(employeeRepository.getEmployeeList().size() - 1)));
	}

	@org.junit.jupiter.api.Test
	void addEmployeeBVA29() {
		Employee newEmployee = new Employee("Adam","Mara", "1234567890123", DidacticFunction.LECTURER, "0");
		assertFalse(employeeValidator.isValid(newEmployee));
		assertFalse(employeeRepository.addEmployee(newEmployee)==true);
		assertNotEquals(7, employeeRepository.getEmployeeList().size());
		assertFalse(newEmployee.equals(employeeRepository.getEmployeeList().get(employeeRepository.getEmployeeList().size() - 1)));
	}
}
