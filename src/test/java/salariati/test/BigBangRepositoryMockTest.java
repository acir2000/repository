package salariati.test;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import salariati.model.DidacticFunction;
import salariati.model.Employee;
import salariati.repository.mock.EmployeeRepositoryMock;
import salariati.validator.EmployeeValidator;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

class BigBangRepositoryMockTest {

    private EmployeeRepositoryMock employeeRepository;
    private EmployeeValidator employeeValidator;

    @BeforeEach
    void setUp() {
        employeeRepository = new EmployeeRepositoryMock();
        employeeRepository.clearRepositoryMock();
        employeeValidator  = new EmployeeValidator();
    }



    @Test
    void unitTestC() {
        Employee employee1 = new Employee("Adam","Corina", "1234567890123", DidacticFunction.LECTURER, "2000");
        Employee employee2 = new Employee("Adam","Mihai", "1234567890124", DidacticFunction.ASISTENT, "2500");

        employeeRepository.addEmployee(employee1);
        employeeRepository.addEmployee(employee2);
        List<Employee> sortedList=employeeRepository.getOrderedEmployeeList();
        assertEquals(2, sortedList.size());
        assertEquals(employee2,sortedList.get(0));
        assertEquals(employee1,sortedList.get(1));

    }

    @Test
    void unitTestA() {
        Employee newEmployee = new Employee("Adam","Corina", "1234567890123", DidacticFunction.LECTURER, "2000");
        assertTrue(employeeValidator.isValid(newEmployee));
        assertTrue(employeeRepository.addEmployee(newEmployee)==true);
        assertEquals(1, employeeRepository.getEmployeeList().size());
        assertTrue(newEmployee.equals(employeeRepository.getEmployeeList().get(employeeRepository.getEmployeeList().size() - 1)));
    }

    @Test
    void unitTestB() {
        Employee oldEmployee = new Employee("Adam","Corina", "1234567890123", DidacticFunction.LECTURER, "2000");
        Employee newEmployee = new Employee("Adam","Corina", "1234567890123", DidacticFunction.LECTURER, "2500");

        employeeRepository.addEmployee(oldEmployee);
        assertTrue(employeeValidator.isValid(oldEmployee));
        employeeRepository.modifyEmployee(oldEmployee,newEmployee);
        assertEquals(1, employeeRepository.getEmployeeList().size());
        assertTrue(newEmployee.equals(employeeRepository.getEmployeeList().get(employeeRepository.getEmployeeList().size() - 1)));

    }
    @Test
    void integrationTest() {
        Employee oldEmployee = new Employee("Adam","Corina", "1234567890123", DidacticFunction.LECTURER, "2000");
        employeeRepository.addEmployee(oldEmployee);
        Employee newEmployee = new Employee("Adam","Corina", "1234567890123", DidacticFunction.LECTURER, "2500");
        employeeRepository.modifyEmployee(oldEmployee,newEmployee);
        employeeRepository.addEmployee(oldEmployee);
        List<Employee> sortedList=employeeRepository.getOrderedEmployeeList();
        assertEquals(2, sortedList.size());
        assertEquals(newEmployee,sortedList.get(0));
        assertEquals(oldEmployee,sortedList.get(1));
    }


  }