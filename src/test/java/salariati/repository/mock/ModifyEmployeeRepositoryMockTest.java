package salariati.repository.mock;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import salariati.model.DidacticFunction;
import salariati.model.Employee;
import salariati.repository.interfaces.EmployeeRepositoryInterface;
import salariati.validator.EmployeeValidator;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

class ModifyEmployeeRepositoryMockTest {

    private EmployeeRepositoryMock employeeRepository;
    private EmployeeValidator employeeValidator;

    @BeforeEach
    void setUp() {
        employeeRepository = new EmployeeRepositoryMock();
        employeeRepository.clearRepositoryMock();
        employeeValidator  = new EmployeeValidator();
    }



    @Test
    void modifyEmployeeTC01() {
        Employee oldEmployee = new Employee(null,null,null,null,null);
        Employee newEmployee = new Employee(null,null,null,null,null);

        assertFalse(employeeValidator.isValid(oldEmployee));
        employeeRepository.modifyEmployee(oldEmployee,newEmployee);
        assertEquals(0, employeeRepository.getEmployeeList().size());
    }

    @Test
    void modifyEmployeeTC02() {
        Employee oldEmployee = new Employee("Adam","Corina", "1234567890123", DidacticFunction.LECTURER, "2000");
        Employee newEmployee = new Employee("Adam","Corina", "1234567890123", DidacticFunction.LECTURER, "2500");

        assertTrue(employeeValidator.isValid(oldEmployee));
        employeeRepository.modifyEmployee(oldEmployee,newEmployee);
        assertEquals(0, employeeRepository.getEmployeeList().size());
    }

    @Test
    void modifyEmployeeTC04() {
        Employee oldEmployee = new Employee("Adam","Corina", "1234567890123", DidacticFunction.LECTURER, "2000");
        Employee newEmployee = new Employee("Adam","Corina", "1234567890123", DidacticFunction.LECTURER, "2500");
        Employee employee = new Employee("Adam","Corina", "1234567890123", DidacticFunction.LECTURER, "4000");

        employeeRepository.addEmployee(employee);
        assertTrue(employeeValidator.isValid(oldEmployee));
        employeeRepository.modifyEmployee(oldEmployee,newEmployee);
        assertEquals(1, employeeRepository.getEmployeeList().size());
        assertTrue(employee.equals(employeeRepository.getEmployeeList().get(employeeRepository.getEmployeeList().size() - 1)));

    }

    @Test
    void modifyEmployeeTC05() {
        Employee oldEmployee = new Employee("Adam","Corina", "1234567890123", DidacticFunction.LECTURER, "2000");
        Employee newEmployee = new Employee("Adam","Corina", "1234567890123", DidacticFunction.LECTURER, "2500");

        employeeRepository.addEmployee(oldEmployee);
        assertTrue(employeeValidator.isValid(oldEmployee));
        employeeRepository.modifyEmployee(oldEmployee,newEmployee);
        assertEquals(1, employeeRepository.getEmployeeList().size());
        assertTrue(newEmployee.equals(employeeRepository.getEmployeeList().get(employeeRepository.getEmployeeList().size() - 1)));

    }

  }