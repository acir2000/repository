package salariati.repository.mock;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import salariati.controller.EmployeeController;
import salariati.model.DidacticFunction;
import salariati.model.Employee;
import salariati.repository.interfaces.EmployeeRepositoryInterface;
import salariati.validator.EmployeeValidator;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.*;

class AddEmployeeRepositoryMockTest {

    private EmployeeRepositoryInterface employeeRepository;
    private EmployeeValidator employeeValidator;

    @BeforeEach
    void setUp() {
        employeeRepository = new EmployeeRepositoryMock();
        employeeValidator  = new EmployeeValidator();
    }



    @Test
    void addEmployeeEC1() {
        Employee newEmployee = new Employee("Adam","Corina", "1234567890123", DidacticFunction.LECTURER, "2000");
        assertTrue(employeeValidator.isValid(newEmployee));
        assertTrue(employeeRepository.addEmployee(newEmployee)==true);
        assertEquals(7, employeeRepository.getEmployeeList().size());
        assertTrue(newEmployee.equals(employeeRepository.getEmployeeList().get(employeeRepository.getEmployeeList().size() - 1)));
    }

    @Test
    void addEmployeeEC3() {
        Employee newEmployee = new Employee("A2am","Corina", "1234567890123", DidacticFunction.LECTURER, "2000");
        assertFalse(employeeValidator.isValid(newEmployee));
        assertFalse(employeeRepository.addEmployee(newEmployee)==true);
        assertNotEquals(7, employeeRepository.getEmployeeList().size());
        assertFalse(newEmployee.equals(employeeRepository.getEmployeeList().get(employeeRepository.getEmployeeList().size() - 1)));
    }

    @Test
    void addEmployeeEC4() {
        Employee newEmployee = new Employee("Ad","Corina", "1234567890123", DidacticFunction.LECTURER, "2000");
        assertFalse(employeeValidator.isValid(newEmployee));
        assertFalse(employeeRepository.addEmployee(newEmployee)==true);
        assertNotEquals(7, employeeRepository.getEmployeeList().size());
        assertFalse(newEmployee.equals(employeeRepository.getEmployeeList().get(employeeRepository.getEmployeeList().size() - 1)));
    }

    @Test
    void addEmployeeEC6() {
        Employee newEmployee = new Employee("Adam","C0rina", "1234567890123", DidacticFunction.LECTURER, "2000");
        assertFalse(employeeValidator.isValid(newEmployee));
        assertFalse(employeeRepository.addEmployee(newEmployee)==true);
        assertNotEquals(7, employeeRepository.getEmployeeList().size());
        assertFalse(newEmployee.equals(employeeRepository.getEmployeeList().get(employeeRepository.getEmployeeList().size() - 1)));
    }

    @Test
    void addEmployeeEC11() {
        Employee newEmployee = new Employee("Adam","Corina", "123456789aaaa", DidacticFunction.LECTURER, "2000");
        assertFalse(employeeValidator.isValid(newEmployee));
        assertFalse(employeeRepository.addEmployee(newEmployee)==true);
        assertNotEquals(7, employeeRepository.getEmployeeList().size());
        assertFalse(newEmployee.equals(employeeRepository.getEmployeeList().get(employeeRepository.getEmployeeList().size() - 1)));
    }

    @Test
    void addEmployeeEC16() {
        Employee newEmployee = new Employee("Adam","Corina", "1234567890123", DidacticFunction.LECTURER, "-100");
        assertFalse(employeeValidator.isValid(newEmployee));
        assertFalse(employeeRepository.addEmployee(newEmployee)==true);
        assertNotEquals(7, employeeRepository.getEmployeeList().size());
        assertFalse(newEmployee.equals(employeeRepository.getEmployeeList().get(employeeRepository.getEmployeeList().size() - 1)));
    }

    @Test
    void addEmployeeBVA4() {
        Employee newEmployee = new Employee("A","Corina", "1234567890123", DidacticFunction.LECTURER, "100");
        assertFalse(employeeValidator.isValid(newEmployee));
        assertFalse(employeeRepository.addEmployee(newEmployee)==true);
        assertNotEquals(7, employeeRepository.getEmployeeList().size());
        assertFalse(newEmployee.equals(employeeRepository.getEmployeeList().get(employeeRepository.getEmployeeList().size() - 1)));
    }

    @Test
    void addEmployeeBVA13() {
        Employee newEmployee = new Employee("Adam","Ma", "1234567890123", DidacticFunction.LECTURER, "100");
        assertFalse(employeeValidator.isValid(newEmployee));
        assertFalse(employeeRepository.addEmployee(newEmployee)==true);
        assertNotEquals(7, employeeRepository.getEmployeeList().size());
        assertFalse(newEmployee.equals(employeeRepository.getEmployeeList().get(employeeRepository.getEmployeeList().size() - 1)));
    }

    @Test
    void addEmployeeBVA20() {
        Employee newEmployee = new Employee("Adam","Mara", "123456789012", DidacticFunction.LECTURER, "100");
        assertFalse(employeeValidator.isValid(newEmployee));
        assertFalse(employeeRepository.addEmployee(newEmployee)==true);
        assertNotEquals(7, employeeRepository.getEmployeeList().size());
        assertFalse(newEmployee.equals(employeeRepository.getEmployeeList().get(employeeRepository.getEmployeeList().size() - 1)));
    }

    @Test
    void addEmployeeBVA24() {
        Employee newEmployee = new Employee("Adam","Corina", "1234567890123", DidacticFunction.ASISTENT, "200");
        assertTrue(employeeValidator.isValid(newEmployee));
        assertTrue(employeeRepository.addEmployee(newEmployee)==true);
        assertEquals(7, employeeRepository.getEmployeeList().size());
        assertTrue(newEmployee.equals(employeeRepository.getEmployeeList().get(employeeRepository.getEmployeeList().size() - 1)));
    }

    @Test
    void addEmployeeBVA33() {
        Employee newEmployee = new Employee("Adam","Mara", "1234567890123", DidacticFunction.LECTURER, "100001");
        assertFalse(employeeValidator.isValid(newEmployee));
        assertFalse(employeeRepository.addEmployee(newEmployee)==true);
        assertNotEquals(7, employeeRepository.getEmployeeList().size());
        assertFalse(newEmployee.equals(employeeRepository.getEmployeeList().get(employeeRepository.getEmployeeList().size() - 1)));
    }

    @Test
    void addEmployeeBVA29() {
        Employee newEmployee = new Employee("Adam","Mara", "1234567890123", DidacticFunction.LECTURER, "0");
        assertFalse(employeeValidator.isValid(newEmployee));
        assertFalse(employeeRepository.addEmployee(newEmployee)==true);
        assertNotEquals(7, employeeRepository.getEmployeeList().size());
        assertFalse(newEmployee.equals(employeeRepository.getEmployeeList().get(employeeRepository.getEmployeeList().size() - 1)));
    }
}